<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <!-- ====================================================================== -->
    <!-- P A R E N T  P R O J E C T  D E S C R I P T I O N                      -->
    <!-- ====================================================================== -->
    <parent>
        <groupId>com.intelligencebank.aem</groupId>
        <artifactId>aem-connector</artifactId>
        <version>2.1.8</version>
    </parent>

    <!-- ====================================================================== -->
    <!-- P R O J E C T  D E S C R I P T I O N                                   -->
    <!-- ====================================================================== -->
    <artifactId>aem-connector.reference</artifactId>
    <packaging>content-package</packaging>
    <name>IntelligenceBank AEM Connector - Reference</name>
    <description>Reference Site for IntelligenceBank AEM Connector</description>

    <!-- ====================================================================== -->
    <!-- B U I L D   D E F I N I T I O N                                        -->
    <!-- ====================================================================== -->
    <build>
        <plugins>
            <!-- ====================================================================== -->
            <!-- V A U L T   P A C K A G E   P L U G I N S                              -->
            <!-- ====================================================================== -->
            <plugin>
                <groupId>org.apache.jackrabbit</groupId>
                <artifactId>filevault-package-maven-plugin</artifactId>
                <extensions>true</extensions>
                <configuration>
                    <group>IntelligenceBank</group>
                    <packageType>container</packageType>
                    <!-- skip sub package validation for now as some vendor packages like CIF apps will not pass -->
                    <skipSubPackageValidation>true</skipSubPackageValidation>
                    <embeddeds>
                        <embedded>
                            <groupId>com.intelligencebank.aem</groupId>
                            <artifactId>aem-connector.all</artifactId>
                            <type>content-package</type>
                            <target>/apps/ib-reference/install</target>
                        </embedded>
                        <embedded>
                            <groupId>com.adobe.aem.guides</groupId>
                            <artifactId>aem-guides-wknd.all</artifactId>
                            <type>content-package</type>
                            <target>/apps/ib-reference/install</target>
                        </embedded>
                    </embeddeds>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.day.jcr.vault</groupId>
                <artifactId>content-package-maven-plugin</artifactId>
                <extensions>true</extensions>
                <configuration>
                    <verbose>true</verbose>
                    <failOnError>true</failOnError>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-clean-plugin</artifactId>
                <executions>
                    <execution>
                        <id>auto-clean</id>
                        <phase>initialize</phase>
                        <goals>
                            <goal>clean</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <id>central</id>
            <name>default-maven-virtual</name>
            <url>
                https://bitbucket.org/IB-IntelligenceBank/intelligencebank-connector-for-adobe-experience-manager-public/raw/master/releases
            </url>
        </repository>
        <repository>
            <snapshots />
            <id>snapshots</id>
            <name>default-maven-virtual</name>
            <url>
                https://bitbucket.org/IB-IntelligenceBank/intelligencebank-connector-for-adobe-experience-manager-public/raw/master/releases
            </url>
        </repository>
    </repositories>

    <profiles>
        <profile>
            <id>autoInstallSinglePackage</id>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>
            <build>
                <plugins>
                    <plugin>
                        <groupId>com.day.jcr.vault</groupId>
                        <artifactId>content-package-maven-plugin</artifactId>
                        <executions>
                            <execution>
                                <id>install-package</id>
                                <goals>
                                    <goal>install</goal>
                                </goals>
                                <configuration>
                                    <targetURL>http://${aem.host}:${aem.port}/crx/packmgr/service.jsp</targetURL>
                                    <failOnError>true</failOnError>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <dependencies>
        <dependency>
            <groupId>com.adobe.aem.guides</groupId>
            <artifactId>aem-guides-wknd.all</artifactId>
            <version>0.2.0</version>
            <type>content-package</type>
        </dependency>

        <dependency>
            <groupId>com.intelligencebank.aem</groupId>
            <artifactId>aem-connector.all</artifactId>
            <version>${project.version}</version>
            <type>content-package</type>
        </dependency>
    </dependencies>
</project>

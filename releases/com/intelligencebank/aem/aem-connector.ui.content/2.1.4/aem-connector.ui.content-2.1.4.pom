<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <!-- ====================================================================== -->
    <!-- P A R E N T  P R O J E C T  D E S C R I P T I O N                      -->
    <!-- ====================================================================== -->
    <parent>
        <groupId>com.intelligencebank.aem</groupId>
        <artifactId>aem-connector</artifactId>
        <version>2.1.4</version>
        <relativePath>../pom.xml</relativePath>
    </parent>

    <!-- ====================================================================== -->
    <!-- P R O J E C T  D E S C R I P T I O N                                   -->
    <!-- ====================================================================== -->
    <artifactId>aem-connector.ui.content</artifactId>
    <packaging>content-package</packaging>
    <name>IntelligenceBank AEM Connector - UI content</name>
    <description>UI content package for IntelligenceBank AEM Connector</description>

    <!-- ====================================================================== -->
    <!-- B U I L D   D E F I N I T I O N                                        -->
    <!-- ====================================================================== -->
    <build>
        <plugins>
            <!-- ====================================================================== -->
            <!-- V A U L T   P A C K A G E   P L U G I N S                              -->
            <!-- ====================================================================== -->
            <plugin>
                <groupId>org.apache.jackrabbit</groupId>
                <artifactId>filevault-package-maven-plugin</artifactId>
                <configuration>
                    <properties>
                        <cloudManagerTarget>none</cloudManagerTarget>
                    </properties>
                    <group>IntelligenceBank</group>
                    <name>aem-connector.ui.content</name>
                    <packageType>content</packageType>
                    <validatorsSettings>
                        <jackrabbit-filter>
                            <options>
                                <validRoots>/conf/global/settings</validRoots>
                            </options>
                        </jackrabbit-filter>
                    </validatorsSettings>
                    <dependencies>
                        <dependency>
                            <groupId>com.intelligencebank.aem</groupId>
                            <artifactId>aem-connector.ui.apps</artifactId>
                            <version>${project.version}</version>
                        </dependency>
                    </dependencies>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.day.jcr.vault</groupId>
                <artifactId>content-package-maven-plugin</artifactId>
                <extensions>true</extensions>
                <configuration>
                    <verbose>true</verbose>
                    <failOnError>true</failOnError>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <!-- ====================================================================== -->
    <!-- D E P E N D E N C I E S                                                -->
    <!-- ====================================================================== -->
    <dependencies>
        <dependency>
            <groupId>com.intelligencebank.aem</groupId>
            <artifactId>aem-connector.core</artifactId>
            <version>${project.version}</version>
        </dependency>
        <dependency>
            <groupId>com.intelligencebank.aem</groupId>
            <artifactId>aem-connector.ui.apps</artifactId>
            <version>${project.version}</version>
            <type>zip</type>
        </dependency>
        <dependency>
            <groupId>com.adobe.aem</groupId>
            <artifactId>aem-sdk-api</artifactId>
        </dependency>
    </dependencies>
</project>
